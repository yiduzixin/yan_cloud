# yan_cloud

#### 介绍
岩云是一款使用flask+minio实现的简易云存储，特点：1、通过thumbor处理上传的原图，可实现访问不同尺寸的图片；2、上传的视频文件通过ffmpeg可自动转换mp4文件进行存储。

#### 软件架构
![structure](static/images/structure.png)

#### 安装教程

1.  在CentOS 7.9上安装python 3.9
```shell
wget https://www.python.org/ftp/python/3.9.7/Python-3.9.7.tgz
tar zxf Python-3.9.7.tgz
cd Python-3.9.7
./configure
make
make install
```

2.  Minio安装
```shell
wget https://dl.min.io/server/minio/release/linux-amd64/minio
chmod +x minio
cp minio /usr/bin/
export MINIO_ACCESS_KEY=admin
export MINIO_SECRET_KEY=123456

mkdir /home/minio_file/file_1
mkdir /home/minio_file/file_2
mkdir /home/minio_file/file_3
mkdir /home/minio_file/file_4
mkdir /home/minio_log

# 启动
nohup minio server --address 0.0.0.0:9001 http://192.168.1.30:9001/home/minio_file/file_1 http://192.168.1.30:9002/home/minio_file/file_2 http://192.168.1.30:9003/home/minio_file/file_3 http://192.168.1.30:9004/home/minio_file/file_4 > /home/minio_log/minio_1.log 2>&1 &
nohup minio server --address 0.0.0.0:9002 http://192.168.1.30:9001/home/minio_file/file_1 http://192.168.1.30:9002/home/minio_file/file_2 http://192.168.1.30:9003/home/minio_file/file_3 http://192.168.1.30:9004/home/minio_file/file_4 > /home/minio_log/minio_2.log 2>&1 &
nohup minio server --address 0.0.0.0:9003 http://192.168.1.30:9001/home/minio_file/file_1 http://192.168.1.30:9002/home/minio_file/file_2 http://192.168.1.30:9003/home/minio_file/file_3 http://192.168.1.30:9004/home/minio_file/file_4 > /home/minio_log/minio_3.log 2>&1 &
nohup minio server --address 0.0.0.0:9004 http://192.168.1.30:9001/home/minio_file/file_1 http://192.168.1.30:9002/home/minio_file/file_2 http://192.168.1.30:9003/home/minio_file/file_3 http://192.168.1.30:9004/home/minio_file/file_4 > /home/minio_log/minio_4.log 2>&1 &
```
**访问验证：** http://192.168.1.30:9001
<br/>
<br/>
3.  thumbor安装
```shell
pip install -i https://mirrors.aliyun.com/pypi/simple/ --trusted-host mirrors.aliyun.com libthumbor==1.3.2
pip install -i https://mirrors.aliyun.com/pypi/simple/ --trusted-host mirrors.aliyun.com webcolors==1.10
pip install -i https://mirrors.aliyun.com/pypi/simple/ --trusted-host mirrors.aliyun.com thumbor
# 配置文件
cat thumbor.conf
    '''
    SECURITY_KEY = 'yy_87'
    ALLOW_UNSAFE_URL = True
    STORAGE_EXPIRATION_SECONDS = 0
    RESULT_STORAGE_EXPIRATION_SECONDS = 1
    TC_AWS_REGION='us-east-1'
    TC_AWS_ENDPOINT='http://192.168.1.30:9001'
    LOADER='tc_aws.loaders.s3_loader'
    '''
# 启动thumbor
thumbor --port=8888 --conf=/etc/thumbor.conf
```
<br/>
4. 安装ffmpeg

```shell
wget http://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz
tar zxf yasm-1.3.0.tar.gz
cd yasm-1.3.0
./configure
make && make install
cd ../
wget https://ffmpeg.org/releases/ffmpeg-3.3.3.tar.bz2
tar xjvf ffmpeg-3.3.3.tar.bz2
cd ffmpeg-3.3.3
./configure --prefix=/usr/local/ffmpeg --enable-shared
make && make install
# 将/usr/local/ffmpeg/lib加入到系统库中（否则执行ffmpeg -version，会报找不到 libavdevice.so）
echo '/usr/local/ffmpeg/lib' > /etc/ld.so.conf.d/ffmpeg.conf
ldconfig
# 将ffmpeg添加到环境变量
cat /etc/profile
    '''
    PATH=$PATH:$HOME/bin:/usr/local/ffmpeg/bin
    '''
# 重新加载环境变量
source /etc/profile
```

5. 部署yan_cloud

```shell
# 拉取代码
git clone https://gitee.com/yiduzixin/yan_cloud.git
cd yan_cloud
# 建立项目的虚拟环境
pip3 install virtualenv -i https://pypi.doubanio.com/simple/ --trusted-host pypi.doubanio.com
virtualenv venv
# 进入虚拟环境
source venv/bin/activate
# 安装插件
pip3 install -r requirements.txt
# 配置uwsgi.ini
cat uwsgi.ini
'''
[uwsgi]
module = app
callable = app

socket = :8000
master = true
processes = 4
threads = 4
pidfile = /var/run/uwsgi.pid

req-logger = file:/var/log/uwsgi/req.log
logger = file:/var/log/uwsgi/err.log

die-on-term = true
'''
# 新建日志目录
mkdir -p /var/log/uwsgi
```

6. nginx配置

```shell
upstream isfile {
    server 192.168.1.30:9001;
    server 192.168.1.30:9002;
    server 192.168.1.30:9003;
    server 192.168.1.30:9004;
}
server {
    listen 80;
    server_name xxxx.yan_cloud.xxx;
    error_log   /var/log/nginx/xxxx.yan_cloud.xxx_error.log;
    access_log  /var/log/nginx/xxxx.yan_cloud.xxx_access.log  main;


    location ^~ /img/ {
        proxy_set_header X-Forwarded-Host $host:$server_port;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header URI $uri;
        proxy_pass http://127.0.0.1:8888/;
    }

    location ^~ /minio/ {
        proxy_set_header X-Forwarded-Host $host:$server_port;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header URI $uri;
        proxy_pass http://isfile/;
    }

    location / {
        include uwsgi_params;
        uwsgi_pass 127.0.0.1:8000;
        uwsgi_param UWSGI_CHDIR /home/yan_cloud;
        uwsgi_param UWSGI_SCRIPT yan_cloud.app;
        client_max_body_size 512m;
    }
}
```
