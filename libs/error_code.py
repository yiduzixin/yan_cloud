#!/usr/bin/env python
# -*-coding:utf-8-*-
# @Time : 2021/9/16 11:03
# @Author : Li Yan
from libs.error import APIException


class ServerError(APIException):
    code = 500
    msg = 'sorry, we made a mistake (*￣︶￣)!'
    error_code = 999


class FileTypeException(APIException):
    code = 400
    msg = '请检查后缀名是否合法!'
    error_code = 1001


class AccessForbidden(APIException):
    code = 403
    msg = '验证不通过!'
    error_code = 1002


class UploadTypeError(APIException):
    code = 415
    msg = '上传类型选择错误!'
    error_code = 1003
