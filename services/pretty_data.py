#!/usr/bin/env python
# -*-coding:utf-8-*-
# @Time : 2021/9/24 10:51
# @Author : Li Yan
from flask import current_app

from utils.mix_tool import encode_url


def pretty_data(upload_type: str, file_name: str, date_format: str) -> dict:
    r = dict()
    if upload_type == 'image':
        r = image_data(file_name, date_format)
    elif upload_type == 'video':
        r = video_data(file_name, date_format)
    elif upload_type == 'file':
        r = file_data(file_name, date_format)
    return r


def _scale_pic(scale, file_name, date_format):
    bucket = current_app.config['BUCKET']
    thumbor_host = current_app.config['THUMBOR_HOST']
    scale_uri = 'filters:' + scale + bucket + '/images/' + date_format + '/' + file_name
    encode_uri = encode_url(scale_uri)
    full_link = thumbor_host + '/' + encode_uri + '/' + scale_uri
    return {"encode_uri": encode_uri, "scale_uri": scale_uri, "full_url": full_link}


def image_data(file_name: str, date_format: str) -> dict:
    r = dict()
    o_size = _scale_pic('proportion(0)/', file_name, date_format)
    s_size = _scale_pic('proportion(0.5)/', file_name, date_format)
    m_size = _scale_pic('proportion(0.7)/', file_name, date_format)
    l_size = _scale_pic('proportion(1.3)/', file_name, date_format)
    xl_size = _scale_pic('proportion(1.5)/', file_name, date_format)
    xxl_size = _scale_pic('proportion(2)/', file_name, date_format)

    r['o_size'] = o_size
    r['s_size'] = s_size
    r['m_size'] = m_size
    r['l_size'] = l_size
    r['xl_size'] = xl_size
    r['xxl_size'] = xxl_size

    return r


def video_data(file_name: str, date_format: str) -> dict:
    bucket = current_app.config['BUCKET']
    request_url = current_app.config['REQUEST_URL']
    r = dict()
    r['file_url'] = '/'.join([request_url, bucket, 'video', date_format, file_name])
    return r


def file_data(file_name: str, date_format: str) -> dict:
    bucket = current_app.config['BUCKET']
    request_url = current_app.config['REQUEST_URL']
    r = dict()
    r['file_url'] = '/'.join([request_url, bucket, 'files', date_format, file_name])
    return r
