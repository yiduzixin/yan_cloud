#!/usr/bin/env python
# -*-coding:utf-8-*-
# @Time : 2021/9/22 15:07
# @Author : Li Yan
import threading

from flask import current_app

from services.save2minio import save2minio
from services.save2tmp import save2tmp


def save2cloud(uploadtype: str, master_uuid: str, logo_uuid: str, date_format: str) -> str:
    lock = threading.Lock()
    file_name = save2tmp(lock, uploadtype, master_uuid, logo_uuid)
    app = current_app._get_current_object()
    thr = threading.Thread(target=save2minio, args=[lock, app, uploadtype, file_name, date_format])
    thr.start()
    return file_name
