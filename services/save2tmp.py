#!/usr/bin/env python
# -*-coding:utf-8-*-
# @Time : 2021/9/15 16:00
# @Author : Li Yan
import threading

from utils.file_tool import get_file, get_name
from utils.img_tool import water2image
from utils.video_tool import video2mp4


def save2tmp(lock: object, uploadtype: str, master_uuid: str, logo_uuid: str) -> str:
    filename = ''
    if uploadtype == 'image':
        filename = image2tmp(master_uuid, logo_uuid)
    elif uploadtype == 'video':
        filename = video2tmp(lock, master_uuid, logo_uuid)
    elif uploadtype == 'file':
        filename = all2tmp(master_uuid)
    return filename


def all2tmp(master_uuid: str) -> str:
    filepath = get_file(master_uuid)
    return get_name(filepath)


def video2tmp(lock: object, master_uuid: str, logo_uuid: str) -> str:
    master_file = get_file(master_uuid)
    logo_file = get_file(logo_uuid)
    thr = threading.Thread(target=video2mp4, args=[lock, master_file, logo_file])
    thr.start()
    return master_uuid + '.mp4'


def image2tmp(master_uuid: str, logo_uuid: str) -> str:
    master_file = get_file(master_uuid)
    logo_file = get_file(logo_uuid)
    if logo_file:
        water2image(master_file, logo_file)
    return get_name(master_file)
