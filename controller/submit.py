#!/usr/bin/env python
# -*-coding:utf-8-*-
# @Time : 2021/9/15 14:49
# @Author : Li Yan
import uuid

from flask import Blueprint, request, render_template

from libs.validate import validate
from services.pretty_data import pretty_data
from services.save2cloud import save2cloud
from utils.mix_tool import get_date_str

submit = Blueprint('submit', __name__)


@submit.route('/', methods=['GET', 'POST'])
def get_form():
    if request.method == 'POST':
        validate(request)
        upload_type = request.form['upload_type']
        date_format = get_date_str()
        file_name = save2cloud(upload_type, request.form['master_uuid'], request.form['logo_uuid'], date_format)
        r = pretty_data(upload_type, file_name, date_format)
        return r
    master_uuid = str(uuid.uuid4())
    logo_uuid = str(uuid.uuid4())
    return render_template("index.html", master_uuid=master_uuid, logo_uuid=logo_uuid)
