#!/usr/bin/env python
# -*-coding:utf-8-*-
# @Time : 2021/9/15 11:50
# @Author : Li Yan
from flask import Blueprint

from controller.get_file import getfile
from controller.submit import submit

controller = Blueprint("controller", __name__)
controller.register_blueprint(submit)
controller.register_blueprint(getfile)
