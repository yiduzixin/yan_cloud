#!/usr/bin/env python
# -*-coding:utf-8-*-
# @Time : 2021/9/16 8:53
# @Author : Li Yan
import os
from glob import glob

from flask import current_app


def get_file(_uuid: str) -> str:
    tmp_path = os.path.join(os.getcwd(), current_app.config['TMP_FOLDER'], _uuid + '.*')
    if files := glob(tmp_path):
        return files[0]
    return ''


def get_name(path: str) -> str:
    return os.path.split(path)[1]


def new_name(filename: str, newname: str) -> str:
    extname: str = ext_name(filename)
    return (newname + '.' + extname) if extname else newname


def ext_name(filename: str) -> str:
    return filename.rsplit('.', 1)[1]
