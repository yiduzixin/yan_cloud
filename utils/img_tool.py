#!/usr/bin/env python
# -*-coding:utf-8-*-
# @Time : 2021/9/17 15:19
# @Author : Li Yan
import os

from PIL import Image

from utils.logo_tool import shrink_img


def water2image(image: str, logo: str) -> None:
    img = Image.open(image)
    img_x, img_y = img.size
    shrink_img(logo, img_y, 20)
    water = Image.open(logo)
    water_x, water_y = water.size
    img.paste(water, (img_x - water_x, img_y - water_y), water)
    img.save(image)
    img.close()
    water.close()
    os.remove(logo)
