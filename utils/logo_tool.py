#!/usr/bin/env python
# -*-coding:utf-8-*-
# @Time : 2021/9/22 10:38
# @Author : Li Yan
import math

from PIL import Image


def shrink_img(image: str, height: int, scale: int = 10) -> None:
    img = Image.open(image)
    img_width = img.size[0]
    img_height = img.size[1]
    ratio = height * (scale * 0.01) / img_height
    n_width = math.ceil(img_width * ratio)
    n_height = math.ceil(img_height * ratio)
    temp = img.resize((n_width, n_height), Image.ANTIALIAS)
    temp.save(image)
    img.close()
