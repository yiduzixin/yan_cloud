#!/usr/bin/env python
# -*-coding:utf-8-*-
# @Time : 2021/9/23 10:28
# @Author : Li Yan
import base64
import hashlib
import hmac
import time

from flask import current_app


def get_date_str():
    return time.strftime("%Y/%m/%d", time.localtime())


def encode_url(image_url):
    message = image_url
    key = current_app.config['THUMBOR_KEY']
    b_message = str.encode(message)
    b_key = str.encode(key)
    digester = hmac.new(b_key, b_message, hashlib.sha1)
    signature = digester.digest()
    url_safe_sign = base64.urlsafe_b64encode(signature)
    return url_safe_sign.decode()
