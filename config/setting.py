DEBUG = True    # 设置flask是否为调试模式，默认True（为调试模式），选项 True/False
TMP_FOLDER = 'tmp'  # 存放上传的临时待处理文件，当文件成功上传到minio之后，临时文件被删除

ACCESS_KEY = 'yanyun'   # 岩云上传账号
SECRET_KEY = '******'   # 岩云下载密码

THUMBOR_KEY = '******'  # thumbor访问密钥
THUMBOR_HOST = 'http://xxx.xx.xx.xx'    # Thumbor访问地址

BUCKET = 'yanyun'   # 向存储管理员申请bucket
REQUEST_URL = 'http://xxx.xx.xx.xx'    # minio文件访问链接
MINIO_ENDPOINT = 'minio_host:9001'  # minio上传接口
MINIO_ACCESS_KEY = 'xxxxx'     # minio访问账号
MINIO_SECRET_KEY = 'xxxxxxxxx'    # minio访问密码
MINIO_SECURE = False            # 设置非安全模式访问minio

UPLOAD_TYPE = {'image', 'video', 'file'}    # 上传文件要选择上传类型，普通文件就选file
FILE_EXTENSIONS = {'doc', 'docx', 'pdf', 'rar', 'zip'}  # 允许上传的文档类型
IMAGE_EXTENSIONS = {'png', 'jpg', 'ico', 'gif', 'bmp'}  # 允许上传的图片类型
VIDEO_EXTENSIONS = {'mp4', 'flv', 'wmv', 'webm', 'mkv'}  # 允许上传的视频类型
ALL_TYPES = FILE_EXTENSIONS | IMAGE_EXTENSIONS | VIDEO_EXTENSIONS   # 所有文件上传格式
