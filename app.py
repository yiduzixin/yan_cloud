#!/usr/bin/env python
# -*-coding:utf-8-*-
# @Time : 2021/9/15 11:41
# @Author : Li Yan
import os

from flask import Flask
from werkzeug.exceptions import HTTPException

from controller import controller
from libs.error import APIException
from libs.error_code import ServerError

app = Flask(__name__)
app.config.from_object('config.setting')
TMP_FOLDER = app.config['TMP_FOLDER']
if not os.path.exists(TMP_FOLDER):
    os.makedirs(TMP_FOLDER)
app.register_blueprint(controller)


@app.errorhandler(Exception)
def framework_error(e):
    if isinstance(e, APIException):
        return e
    if isinstance(e, HTTPException):
        code = e.code
        msg = e.description
        error_code = 1007
        return APIException(msg, code, error_code)
    else:
        if not app.config['DEBUG']:
            return ServerError()
        else:
            raise e


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, threaded=True)
